﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using Quartz;
using Quartz.Impl;
using Quartz.Spi;

namespace QuartzDemo
{
    class Program
    {
        private static ServiceProvider _serviceProvider;
        private static IScheduler _scheduler;

        async static Task Main(string[] args)
        {
            RegisterServices();
            await StartJobs();
            await Task.Delay(20 * 1000);
            await ShutdownJobs();
            DisposeServices();
        }

        private async static Task ShutdownJobs()
        {
            await _scheduler.Shutdown();
        }

        private static void RegisterServices()
        {
            var services = new ServiceCollection();
            services.AddSingleton<IJobFactory, SingletonJobFactory>();
            services.AddSingleton<ISchedulerFactory, StdSchedulerFactory>();
            services.AddSingleton<HelloWorldJob>();
            services.AddSingleton(new JobSchedule(
                jobType: typeof(HelloWorldJob),
                cronExpression: "0/5 * * * * ?"
                //https://www.quartz-scheduler.net/documentation/quartz-3.x/tutorial/crontriggers.html#example-cron-expressions
            ));
            _serviceProvider = services.BuildServiceProvider(true);
        }

        private static void DisposeServices()
        {
            if (_serviceProvider == null)
            {
                return;
            }
            if (_serviceProvider is IDisposable)
            {
                ((IDisposable)_serviceProvider).Dispose();
            }
        }

        private async static Task StartJobs()
        {
            _scheduler = await _serviceProvider.GetService<ISchedulerFactory>().GetScheduler();
            var jobSchedules = _serviceProvider.GetServices<JobSchedule>();
            
            foreach (var jobSchedule in jobSchedules)
            {
                var jobDetail = GetJobDetail(jobSchedule);
                var trigger = GetJobTrigger(jobSchedule);

                await _scheduler.ScheduleJob(jobDetail, trigger);
            }
            
            await _scheduler.Start();
        }

        private static ITrigger GetJobTrigger(JobSchedule schedule)
        {
            return TriggerBuilder
                .Create()
                .WithIdentity($"{schedule.JobType.FullName}.trigger")
                .WithCronSchedule(schedule.CronExpression)
                .WithDescription(schedule.CronExpression)
                .Build();
        }

        private static IJobDetail GetJobDetail(JobSchedule schedule)
        {
            var jobType = schedule.JobType;
            return JobBuilder
                .Create(jobType)
                .WithIdentity(jobType.FullName)
                .WithDescription(jobType.Name)
                .Build();
        }
    }

}
