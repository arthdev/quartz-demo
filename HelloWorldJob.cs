using System.Threading.Tasks;
using Quartz;

[DisallowConcurrentExecution]
class HelloWorldJob : IJob
{
    public Task Execute(IJobExecutionContext context)
    {
        System.Console.WriteLine(
            "Hello World! With Quartz .NET at " + System.DateTime.Now.ToString("O")
        );
        return Task.CompletedTask;
    }
}